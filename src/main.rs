use futures::{channel::mpsc::channel, prelude::*};
use std::{
    collections::HashSet,
    sync::{atomic, Arc},
    thread,
    time::Duration,
};
use uuid::Uuid;

use bluster::{
    gatt::{
        characteristic,
        characteristic::Characteristic,
        descriptor::Descriptor,
        event::{Event, Response},
        service::Service,
    },
    Peripheral,
};

mod buffer;

#[tokio::main]
async fn main() {
    let coap_gatt_us: Uuid = "8df804b7-3300-496d-9dfa-f8fb40a236bc".parse().unwrap();
    let coap_gatt_uc: Uuid = "2a58fc3f-3c62-4ecc-8167-d66d4d9410c2".parse().unwrap();

    pretty_env_logger::init();

    // CoAP setup

    let mut buf = buffer::CoapBleBuffer::new();

    // This emulates enough of the coap-ace-poc to stay visible in the coap-ace-poc-webapp
    struct AlwaysUnauthorizedWithSomeCborDetails;

    use coap_message::{MinimalWritableMessage, MutableWritableMessage, ReadableMessage};
    use core::convert::Infallible;
    impl coap_handler::Handler for AlwaysUnauthorizedWithSomeCborDetails {
        type RequestData = ();

        type ExtractRequestError = Infallible;
        type BuildResponseError<M: MinimalWritableMessage> = Infallible;

        fn extract_request_data<M: ReadableMessage>(
            &mut self,
            _request: &M,
        ) -> Result<Self::RequestData, Self::ExtractRequestError> {
            Ok(())
        }

        fn estimate_length(&mut self, _request: &Self::RequestData) -> usize {
            50
        }

        fn build_response<M: MutableWritableMessage>(
            &mut self,
            response: &mut M,
            _request: Self::RequestData,
        ) -> Result<(), Infallible> {
            response.set_code(coap_numbers::code::UNAUTHORIZED.try_into().ok().unwrap());
            // being deprecated, but that's still what the application uses
            response
                .add_option_uint(
                    coap_numbers::option::CONTENT_FORMAT
                        .try_into()
                        .ok()
                        .unwrap(),
                    19u8, /* application/ace+cbor */
                )
                .unwrap();
            response
                .set_payload(&cbor_macro::cbor!(
                    // Having a wrong audience keeps things on display, and the AS will err out. If
                    // we picked eg. d00 and thus enabled getting a token, the OSCORE setup would
                    // fail, giving more flickering activity while attempting to do something, but
                    // the lack of an error response at failure time will mean that we don't see
                    // the AS listed.
                    {1: "https://as.coap.amsuess.com/token", 5: "dxx"}))
                .unwrap();
            Ok(())
        }
    }

    use coap_handler_implementations::HandlerBuilder;
    let mut handler = coap_message_demos::full_application_tree(None)
        // We should have a /time that gets POSTed to, but that's ignored anyway
        .at(&["temp"], AlwaysUnauthorizedWithSomeCborDetails);

    // BLE setup

    let (sender_characteristic, receiver_characteristic) = channel(1);

    let gatt_char = Characteristic::new(
        coap_gatt_uc,
        characteristic::Properties::new(
            Some(characteristic::Read(characteristic::Secure::Insecure(
                sender_characteristic.clone(),
            ))),
            Some(characteristic::Write::WithResponse(
                characteristic::Secure::Insecure(sender_characteristic.clone()),
            )),
            Some(sender_characteristic),
            None,
        ),
        None,
        HashSet::<Descriptor>::new(),
    );

    let characteristics = [gatt_char];

    let characteristic_handler = async {
        let notifying = Arc::new(atomic::AtomicBool::new(false));
        let mut rx = receiver_characteristic;
        while let Some(event) = rx.next().await {
            log::trace!("Got event: {:?}", event);
            match event {
                Event::ReadRequest(read_request) => {
                    log::info!(
                        "GATT server got a read request with offset {}!",
                        read_request.offset
                    );
                    let data = buf.access_buffer().to_vec();
                    log::trace!("Responding {:02x?}", data);
                    if data.len() > read_request.mtu as _ {
                        panic!("Should have thought of that before")
                    }
                    read_request.response.send(Response::Success(data)).unwrap();
                    log::info!("GATT server responded");
                }
                Event::WriteRequest(write_request) => {
                    let new_value = write_request.data;
                    log::info!(
                        "GATT server got a write request with offset {} and data {:02x?}!",
                        write_request.offset,
                        new_value,
                    );

                    let b = buf.access_buffer();
                    b.clear();
                    b.extend_from_slice(&new_value)
                        // FIXME this should be handled gracefully (or better yet, without copying)
                        .expect("Received message does not fit in write buffer");

                    write_request
                        .response
                        .send(Response::Success(vec![]))
                        .unwrap();

                    // ... and prepare buf for reading right away (could be in tasks etc, but then
                    // we'd have to fail reading when performed prematurely, and coap-handler isn't
                    // async yet)

                    buf.process_write(&mut handler);
                }
                Event::NotifySubscribe(notify_subscribe) => {
                    log::info!("GATT server got a notify subscription!");
                    let notifying = Arc::clone(&notifying);
                    notifying.store(true, atomic::Ordering::Relaxed);
                    thread::spawn(move || {
                        let mut count = 0;
                        loop {
                            if !(&notifying).load(atomic::Ordering::Relaxed) {
                                break;
                            };
                            count += 1;
                            log::info!("GATT server notifying \"hi {}\"!", count);
                            notify_subscribe
                                .clone()
                                .notification
                                .try_send(format!("hi {}", count).into())
                                .unwrap();
                            thread::sleep(Duration::from_secs(2));
                        }
                    });
                }
                Event::NotifyUnsubscribe => {
                    log::info!("GATT server got a notify unsubscribe!");
                    notifying.store(false, atomic::Ordering::Relaxed);
                }
            };
        }
    };

    let peripheral = Peripheral::new().await.unwrap();
    peripheral
        .add_service(&Service::new(coap_gatt_us, true, characteristics.into()))
        .unwrap();
    let main_fut = async move {
        while !peripheral.is_powered().await.unwrap() {}
        log::info!("Peripheral powered on");
        peripheral.register_gatt().await.unwrap();
        peripheral
            .start_advertising("CoAP-GATT-Demo", &[coap_gatt_us])
            .await
            .unwrap();
        log::info!("Peripheral starting advertising");
        while !peripheral.is_advertising().await.unwrap() {}
        log::info!("done");
    };

    futures::join!(characteristic_handler, main_fut);
}
