// Pick 650 to explore interactions with the MTU
const BUFSIZE: usize = 512;

pub struct CoapBleBuffer(heapless::Vec<u8, BUFSIZE>);

use coap_message_implementations::*;

impl CoapBleBuffer {
    pub const fn new() -> Self {
        Self(heapless::Vec::new())
    }

    pub fn access_buffer(&mut self) -> &mut heapless::Vec<u8, BUFSIZE> {
        &mut self.0
    }

    fn parse<'a>(&'a mut self) -> Option<inmemory::Message<'a>> {
        self.0
            .get(0)
            .map(|code| *code)
            .map(move |code| inmemory::Message::new(code, &self.0[1..]))
    }

    fn write<R>(&mut self, cb: impl FnOnce(&mut inmemory_write::Message) -> R) -> R {
        let buf = self.access_buffer();
        buf.clear();
        buf.resize_default(BUFSIZE)
            .expect("Clearing exactly to available size");
        let (mut code, mut tail) = buf[..].split_first_mut().expect("Constant size > 1");
        let mut writer = inmemory_write::Message::new(&mut code, &mut tail);
        let result = cb(&mut writer);
        let written = writer.finish();
        buf.truncate(1 + written);
        result
    }

    /// Workhorse for gatt_svr_chr_access_coap, doing the actual CoAP processing and writing the
    /// reponse back into the buffer; this will eventually drive a coap-handler and is indicating
    /// CoapBleBuffer's way towards being a full CoAP server.
    ///
    /// Errors are currently indicated on the command line, and practically expressed by writing a
    /// sutiable error code back into the buffer.
    pub fn process_write(&mut self, handler: &mut impl coap_handler::Handler) {
        let extracted = match self.parse() {
            None => {
                self.access_buffer().clear();
                return;
            }
            Some(msg) => handler.extract_request_data(&msg),
        };

        use coap_message::error::RenderableOnMinimal;
        use coap_message::MinimalWritableMessage;
        match extracted {
            Ok(d) => match self.write(|msg| {
                handler
                    .build_response(msg, d)
                    // FIXME: What is wrong about the handler types that just because we passed in
                    // a short-live message type, we can't use the error ... oh...
                    .map_err(|_| {
                        "Error can not be made renderable because it is lifetimed to the
                             short-lived message type"
                    })
            }) {
                Ok(()) => (),
                Err(e) => {
                    log::error!("Fault rendering {:?}", e);
                    self.write(|msg| msg.set_code(coap_numbers::code::INTERNAL_SERVER_ERROR));
                }
            },
            Err(e) => match self.write(|msg| e.render(msg)) {
                Ok(()) => (),
                Err(e2) => {
                    log::error!("Double fault rendering {:?}", e2);
                    self.write(|msg| msg.set_code(coap_numbers::code::INTERNAL_SERVER_ERROR));
                }
            },
        }
    }
}
