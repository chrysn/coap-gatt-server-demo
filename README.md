CoAP over GATT: GATT server demo
================================

This crate runs a GATT server (the role typically taken by small devices)
on a Linux or macOS.

It roughly follows the [current draft] for CoAP over GATT.

On the CoAP side, this runs the [coap-message-demos] full application.

[current draft]: https://www.ietf.org/archive/id/draft-amsuess-core-coap-over-gatt-01.html
[coap-message-demos]: https://crates.io/crates/coap-message-demos

Trying it out
-------------

```shell
$ cargo run
```

* Open the [verdigris demo in BLE bridge mode] in a browser that supports Web Bluetooth (any large one but Firefox and Safari). Note that this won't work from the same device you are running this crate on, as BLE (at least in my experiments) does not loop back.

* Press the Bluetooth "(Re)connect" button and wait for the "CoAP-GATT-Demo" device to show up; select that and continue.

* Press the WebSocket "(Re)connect" button. This creates a connection to coap://rd.coap.amsuess.com/, and (currently, briefly) registers the CoAP-over-BLE device to that resource directory.

* Watch (eg. using [aiocoap]) how the device is registered, and interact with its resources:

```shell
$ aiocoap-client coap+tcp://rd.coap.amsuess.com/resource-lookup/ --observe
<coap://bledevice.proxy.rd.coap.amsuess.com>; ct="0"; title="Landing page",
<coap://bledevice.proxy.rd.coap.amsuess.com/time>; ct="0"; title="Clock",
<coap://bledevice.proxy.rd.coap.amsuess.com/poem>; sz="1338",
<coap://bledevice.proxy.rd.coap.amsuess.com/cbor/1>; ct="60",
<coap://bledevice.proxy.rd.coap.amsuess.com/cbor/2>; ct="60",
<coap://bledevice.proxy.rd.coap.amsuess.com/cbor>; ct="60"
^C
$ aiocoap-client coap://bledevice.proxy.rd.coap.amsuess.com/cbor
{'hidden': False, 'number': 32, 'label': 'Hello', 'list': [1, 2, 3]}
```

[verdigris demo in BLE bridge mode]: https://chrysn.gitlab.io/verdigris/#ble
[aiocoap]: https://aiocoap.readthedocs.io/en/latest/installation.html
